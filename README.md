

## 💻 Identification desk

The device represents an `IO-link` slave device that serves as a identifier and a hub for other IO-link devices. It is meant to be connected to the robot's end-effector (e.g. a gripper), to which it assigns a unique ID number, which is then sent to the IO-link Master.

<p align="center">
  <img src="photos/1.png?raw=true" height="210" title="hover text">
  <img src="photos/2.jpg?raw=true" height="300" title="hover text">
  <img src="photos/3.png?raw=true" height="210" title="hover text">
</p>


## 🔌 Intelligent gripper

This device is an intelligent pneumatic gripper based on the IO-link communication protocol, which is equipped with SPTE-V1R-S6-B-2.5K `Festo` pressure and UM18-211127111 `SICK` ultrasound sensors. The IO-link stack provided by `TEConcept` runs on `STM32L073RZ`.The gripper works in two modes: it can either retransmit to master the value proportional to voltage level received from the sensors or evaluate the data received from the sensors and send boolean values represeting if the defined threshold has been reached. 

<p align="center">
  <img src="photos/5.jpg?raw=true" height="350" title="hover text">

</p>



### 🏛️ Organizations

The work on this project has been carried out at the Testbed for Industry 4.0 at the Czech Institute of Informatics, Robotics and Cybernetics (CIIRC CTU).

<a href="https://www.ciirc.cvut.cz/"><img src="https://www.ciirc.cvut.cz/wp-content/uploads/2017/10/logo_CIIRC_en.svg" height="50"  > </a>



## 🧠  Authors

 

- Github: [@elizaveta_isianova](https://gitlab.com/elizaveta_isianova)
- Github: [@serhii.voronov](https://gitlab.com/serhii.voronov)



---
